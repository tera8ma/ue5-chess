﻿#pragma once

#include <string>

namespace Magic
{
	std::string MagicConcat(const std::string& InA, const std::string& InB);
}
