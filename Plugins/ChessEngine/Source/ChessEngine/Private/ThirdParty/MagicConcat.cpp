﻿#include "MagicConcat.h"

namespace Magic
{
	std::string MagicConcat(const std::string& InA, const std::string& InB)
	{
		return InA + "::Magic::" + InB;
	}

}
