﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "MagicConcatWrapper.h"

#include "ThirdParty/MagicConcat.h"

FString UMagicConcatWrapper::MagicConcat() const
{
	const std::string result = Magic::MagicConcat(TCHAR_TO_UTF8(*AString), TCHAR_TO_UTF8(*BString));

	return FString(UTF8_TO_TCHAR(result.c_str()));
}

void UMagicConcatWrapper::SetA(const FString& InA)
{
	AString = InA;
}

void UMagicConcatWrapper::SetB(const FString& InB)
{
	BString = InB;
}
