﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "CEBoard.h"
#include "ThirdParty/model/Board.h"

#include "ThirdParty/common/extra_utils.h"

namespace CEBoardHelpers
{
	int VectorToPos(const FIntVector2& InValue)
	{
		return InValue.Y << 4 | InValue.X;
	}

	move CreateMove(const FCEMove& InMove, const TUniquePtr<Board>& InBoard)
	{
		move PlayerMove;
		PlayerMove.pos_old = VectorToPos(InMove.FromPosition);
		PlayerMove.pos_new = VectorToPos(InMove.ToPosition);
		
		switch (InMove.MoveType)
		{
		case ECEMoveType::Ordinary:
		case ECEMoveType::Capture:
			PlayerMove.special = MOVE_ORDINARY;
			break;

		case ECEMoveType::CastleLong:
			PlayerMove.special = MOVE_CASTLE_LONG;
			break;

		case ECEMoveType::CastleShort:
			PlayerMove.special = MOVE_CASTLE_SHORT;
			break;

		case ECEMoveType::EnPassant:
			PlayerMove.special = MOVE_EN_PASSANT;
			break;

		case ECEMoveType::Promotion:
			PlayerMove.special = MOVE_PROMOTION;
			PlayerMove.promoted = static_cast<uint8>(InMove.Promotion) * InBoard->to_move; 
			break;

		default:
			PlayerMove.move = MOVE_ERROR;
			break;
		}

		PlayerMove.moved_piece = InBoard->board[PlayerMove.pos_old];
		PlayerMove.content = InBoard->board[PlayerMove.pos_new];

		return PlayerMove;
	}
}

UCEBoard::UCEBoard()
{
	BoardPtr = MakeUnique<Board>(DEFAULT_FEN, false);
	MoveGeneratorPtr = MakeUnique<MoveGenerator>(BoardPtr.Get());
	AllPossibleMoves.Reserve(64);
	UpdateAllPossibleMoves();
}

void UCEBoard::MakeMove(const FCEMove& InMove)
{
	if (InMove.FromPosition == InMove.ToPosition)
	{
		return;
	}
	const move Move = CEBoardHelpers::CreateMove(InMove, BoardPtr);
	if (Move.special == MOVE_ERROR)
	{
		UE_LOG(LogTemp, Log, TEXT("Failed to make move: ''"));
		return;
	}
	BoardPtr->play_move(Move);
	update_board_status(BoardPtr.Get());
	UpdateAllPossibleMoves();
}

FString UCEBoard::GetForsythEdwardsNotation() const
{
	const auto StrNotation = BoardPtr->get_fen();
	return FString{StrNotation.c_str()};
}

ECEGameStatus UCEBoard::GetGameStatus() const
{
	switch (BoardPtr->get_status())
	{
		case STATUS_NORMAL:
			return ECEGameStatus::InProgress;
		case STATUS_CHECK:
			return ECEGameStatus::Check;
		case STATUS_WHITE_MATES:
			return ECEGameStatus::WhiteWin;
		case STATUS_BLACK_MATES:
			return ECEGameStatus::BlackWin;
		case STATUS_STALEMATE:
			return ECEGameStatus::StaleMate;
		case STATUS_DRAW:
			return ECEGameStatus::Draw;
		case STATUS_WHITE_WINS:
			return ECEGameStatus::WhiteWin;
		case STATUS_BLACK_WINS:
			return ECEGameStatus::BlackWin;
		default:
			return ECEGameStatus::InProgress;
	}
}

TArray<FString> UCEBoard::GetAvailableKnightMoves(const FString& InPosition) const
{
	MoveGeneratorPtr->reset();
	MoveGeneratorPtr->generate_moves_knight(get_square(TCHAR_TO_UTF8(*InPosition)));
	MoveGeneratorPtr->sort_moves();

	return GetCurrentMovesFromGenerator();
}

TArray<FString> UCEBoard::GetAvailableKingMoves(const FString& InPosition) const
{
	MoveGeneratorPtr->reset();
	MoveGeneratorPtr->generate_moves_king(get_square(TCHAR_TO_UTF8(*InPosition)));
	MoveGeneratorPtr->generate_castling_moves();
	MoveGeneratorPtr->sort_moves();

	return GetCurrentMovesFromGenerator();
}

TArray<FString> UCEBoard::GetAvailablePawnMoves(const FString& InPosition) const
{
	MoveGeneratorPtr->reset();
	MoveGeneratorPtr->generate_moves_pawn(get_square(TCHAR_TO_UTF8(*InPosition)));
	MoveGeneratorPtr->sort_moves();

	return GetCurrentMovesFromGenerator();
}

TArray<FString> UCEBoard::GetAvailableBishopMoves(const FString& InPosition) const
{
	MoveGeneratorPtr->reset();
	MoveGeneratorPtr->generate_moves_bishop(get_square(TCHAR_TO_UTF8(*InPosition)));
	MoveGeneratorPtr->sort_moves();

	return GetCurrentMovesFromGenerator();
}

TArray<FString> UCEBoard::GetAvailableQueenMoves(const FString& InPosition) const
{
	MoveGeneratorPtr->reset();
	MoveGeneratorPtr->generate_moves_queen(get_square(TCHAR_TO_UTF8(*InPosition)));
	MoveGeneratorPtr->sort_moves();

	return GetCurrentMovesFromGenerator();
}

TArray<FString> UCEBoard::GetAvailableRookMoves(const FString& InPosition) const
{
	MoveGeneratorPtr->reset();
	MoveGeneratorPtr->generate_moves_rook(get_square(TCHAR_TO_UTF8(*InPosition)));
	MoveGeneratorPtr->sort_moves();

	return GetCurrentMovesFromGenerator();
}

void UCEBoard::UpdateAllPossibleMoves()
{
	AllPossibleMoves.Empty();
	MoveGeneratorPtr->generate_all_moves();
	const std::vector<move> Moves = MoveGeneratorPtr->get_all_moves();

	for (const auto& x : Moves)
	{
		const int32 MovePositions = x.pos_old << 8 | x.pos_new;
		AllPossibleMoves.Add(MovePositions);
	}
}

TArray<FString> UCEBoard::GetCurrentMovesFromGenerator() const
{
	const std::vector<move> Moves = MoveGeneratorPtr->get_all_moves();
	TArray<FString> OutputMoves;
	OutputMoves.Reserve(Moves.size());

	for (const auto& x : Moves)
	{
		const std::string& StrMove = square_to_string(x.pos_new);
		const int32 MovePositions = x.pos_old << 8 | x.pos_new;

		if (AllPossibleMoves.Contains(MovePositions))
		{
			const FString& NewMove = FString{StrMove.c_str()};
			OutputMoves.Add(NewMove);	
		}
	}

	return OutputMoves;
}
