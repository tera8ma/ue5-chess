﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "MagicConcatWrapper.generated.h"

/**
 * 
 */
UCLASS()
class CHESSENGINE_API UMagicConcatWrapper : public UObject
{
	GENERATED_BODY()

public:
	FString MagicConcat() const;
	void SetA(const FString& InA);
	void SetB(const FString& InB);

private:
	FString AString;
	FString BString;
};
