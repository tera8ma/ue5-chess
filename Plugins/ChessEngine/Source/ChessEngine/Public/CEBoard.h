﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "CEBoard.generated.h"

UENUM()
enum class ECEMoveType : uint8
{
	None,
	Ordinary,
	Capture,
	CastleLong,
	CastleShort,
	EnPassant,
	Promotion
};

UENUM()
enum class ECEPieceType : uint8
{
	None,
	Pawn,
	Knight,
	Bishop,
	Rook,
	Queen,
	King
};

UENUM()
enum class ECEGameStatus : uint8
{
	WhiteWin,
	BlackWin,
	StaleMate,
	Check,
	Draw,
	InProgress
};

USTRUCT()
struct FCEMove
{
	GENERATED_BODY()

public:
	UPROPERTY()
	FIntVector2 FromPosition;
	UPROPERTY()
	FIntVector2 ToPosition;
	UPROPERTY()
	ECEMoveType MoveType = ECEMoveType::Ordinary;
	UPROPERTY()
	ECEPieceType Promotion = ECEPieceType::None;
};

/**
 * 
 */
UCLASS()
class CHESSENGINE_API UCEBoard : public UObject
{
	GENERATED_BODY()

public:
	UCEBoard();
	void MakeMove(const FCEMove& InMove);
	FString GetForsythEdwardsNotation() const;
	ECEGameStatus GetGameStatus() const;
	
	
	TArray<FString> GetAvailableKnightMoves(const FString& InPosition) const; 
	TArray<FString> GetAvailableKingMoves(const FString& InPosition) const; 
	TArray<FString> GetAvailablePawnMoves(const FString& InPosition) const; 
	TArray<FString> GetAvailableBishopMoves(const FString& InPosition) const; 
	TArray<FString> GetAvailableQueenMoves(const FString& InPosition) const; 
	TArray<FString> GetAvailableRookMoves(const FString& InPosition) const; 
	
private:
	TUniquePtr<class MoveGenerator> MoveGeneratorPtr;
	TUniquePtr<class Board> BoardPtr;
	TSet<int32> AllPossibleMoves;

	void UpdateAllPossibleMoves();
	
	TArray<FString> GetCurrentMovesFromGenerator() const;
};
