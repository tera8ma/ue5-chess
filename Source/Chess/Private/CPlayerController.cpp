﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "CPlayerController.h"

#include "CBoard.h"
#include "CBoardTile.h"
#include "ChessGameState.h"
#include "CPlayerState.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"
#include "UI/CPromotionWindow.h"
#include "EnhancedInputSubsystems.h"

// Sets default values
ACPlayerController::ACPlayerController()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void ACPlayerController::SetInputMapping() const
{
	if (UEnhancedInputLocalPlayerSubsystem* InputSystem = GetLocalPlayer()->GetSubsystem<UEnhancedInputLocalPlayerSubsystem>())
	{
		if (InputMappingContext != nullptr)
		{
			InputSystem->AddMappingContext(InputMappingContext, 0);
		}
	}
}

void ACPlayerController::RemoveInputMapping() const
{
	if (UEnhancedInputLocalPlayerSubsystem* InputSystem = GetLocalPlayer()->GetSubsystem<UEnhancedInputLocalPlayerSubsystem>())
	{
		if (InputMappingContext != nullptr)
		{
			InputSystem->RemoveMappingContext(InputMappingContext);
		}
	}
} 

// Called when the game starts or when spawned
void ACPlayerController::BeginPlay()
{
	Super::BeginPlay();

	FTimerManager& TimerManager = GetWorld()->GetTimerManager();
	
	TimerManager.SetTimer(MouseHoveHandle, FTimerDelegate::CreateUObject(this, &ThisClass::OnMouseHover), HoverRefreshRate, true);
	BoardActor = CastChecked<ACBoard>(UGameplayStatics::GetActorOfClass(GetWorld(), ACBoard::StaticClass()));

	if (!HasAuthority())
	{
		SetInputMapping();
	}
}

// Called every frame
void ACPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (DraggingPiece)
	{
		FHitResult HitResult;
		GetHitResultUnderCursor(ECollisionChannel::ECC_GameTraceChannel1, false, HitResult);

		if (HitResult.bBlockingHit)
		{
			DraggingPiece->SetActorLocation(HitResult.Location + FVector::ZAxisVector * DragHeightOffset);
		}
	}
}

void ACPlayerController::OnMouseHover() const
{
	FHitResult HitResult;
	GetHitResultUnderCursor(ECollisionChannel::ECC_GameTraceChannel1, false, HitResult);

	if (HitResult.bBlockingHit)
	{
		GetBoard()->HoverTile(Cast<ACBoardTile>(HitResult.GetActor()));
	}
	else
	{
		GetBoard()->HoverTile(nullptr);
	}
}

ACBoard* ACPlayerController::GetBoard() const
{
	return BoardActor;
}

bool ACPlayerController::CanMakeMove() const
{
	const ACPlayerState* CPlayerState = GetPlayerState<ACPlayerState>();
	const AChessGameState* CGameState = GetWorld()->GetGameState<AChessGameState>();
	if (CPlayerState == nullptr || CGameState == nullptr)
	{
		return false;
	}

	if (CPlayerState->GetPlayerColor() != CGameState->GetActivePlayerColor())
	{
		return false;
	}
	return true;
}

bool ACPlayerController::CanMovePiece(const ACPiece* InPiece) const
{
	const ACPlayerState* CPlayerState = GetPlayerState<ACPlayerState>();
	if (CPlayerState == nullptr || InPiece == nullptr)
	{
		return false;
	}
	return InPiece->GetColor() == CPlayerState->GetPlayerColor();
}

void ACPlayerController::TryStartDragging()
{
	checkf(DraggingPiece == nullptr, TEXT("Trying to drag to pieces at once"));
	if (!CanMakeMove())
	{
		return;
	}

	FHitResult HitResult;
	GetHitResultUnderCursor(ECollisionChannel::ECC_GameTraceChannel1, false, HitResult);

	if (HitResult.bBlockingHit)
	{
		const ACBoardTile* HitTile = Cast<ACBoardTile>(HitResult.GetActor());
		const FIntVector2& TileCoordinates = GetBoard()->GetTileCoordinates(HitTile);
		const ACPiece* Piece = GetBoard()->GetPieceAt(TileCoordinates);
		if (!CanMovePiece(Piece))
		{
			return;
		}

		DraggingPiece = const_cast<ACPiece*>(Piece);
		RemoveInputMapping();
	}

	GetBoard()->HighlightMoves(DraggingPiece);
}

void ACPlayerController::ShowPromotionWindow(ACPiece* InPiece, ACBoardTile* TileUnderCursor)
{
	PromotionWindow = CreateWidget<UCPromotionWindow>(GetWorld(), PromotionWindowBP.LoadSynchronous());
	if (PromotionWindow)
	{
		PromotionWindow->AddToViewport(1);
		PromotionWindow->OnPromoted.BindLambda([this, InPiece, TileUnderCursor](ECPieceType InPieceType)
		{
			GetBoard()->MakeMove(InPiece, TileUnderCursor, InPieceType);
			GetBoard()->ResetHighlight();

			PromotionWindow->RemoveFromParent();
			PromotionWindow = nullptr;
		});
	}
}

void ACPlayerController::StopDragging()
{
	SetInputMapping();
	if (!DraggingPiece)
	{
		return;
	}

	FHitResult HitResult;
	GetHitResultUnderCursor(ECollisionChannel::ECC_GameTraceChannel1, false, HitResult);

	ACBoardTile* TileUnderCursor = Cast<ACBoardTile>(HitResult.GetActor());

	const FIntVector2 TileCoordinates = GetBoard()->GetTileCoordinates(TileUnderCursor);
	if (DraggingPiece->GetPieceType() == ECPieceType::Pawn && (TileCoordinates.Y == 0 || TileCoordinates.Y == 7))
	{
		ShowPromotionWindow(DraggingPiece, TileUnderCursor);
	}
	else
	{
		GetBoard()->MakeMove(DraggingPiece, TileUnderCursor, ECPieceType::None);
		GetBoard()->ResetHighlight();
	}
	
	DraggingPiece = nullptr;
}




