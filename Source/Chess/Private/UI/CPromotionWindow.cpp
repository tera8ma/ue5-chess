﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/CPromotionWindow.h"

void UCPromotionWindow::SetPromotionResult(const ECPieceType InPiece)
{
	OnPromoted.ExecuteIfBound(InPiece);
}
