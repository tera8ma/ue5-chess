﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "CBoardTile.h"


// Sets default values
ACBoardTile::ACBoardTile()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TileComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("TileMeshComp"));
	SetRootComponent(TileComponent);

	MarkerComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MakerMeshComp"));
	MarkerComponent->SetupAttachment(GetRootComponent());

	HoverComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("HoverMeshComp"));
	HoverComponent->SetupAttachment(GetRootComponent());
}

void ACBoardTile::SetTileState(const EBoardTileState InNewState)
{
	TileState = InNewState;

	BP_UpdateVisualState();
}

EBoardTileState ACBoardTile::GetTileState() const
{
	return TileState;
}

void ACBoardTile::SetTileColor(const EBoardColor InColor)
{
	TileColor = InColor;

	BP_UpdateVisualState();
}

EBoardColor ACBoardTile::GetTileColor() const
{
	return TileColor;
}

void ACBoardTile::SetHover(const bool InValue)
{
	HoverComponent->SetVisibility(InValue);
}

// Called when the game starts or when spawned
void ACBoardTile::BeginPlay()
{
	Super::BeginPlay();

	BP_UpdateVisualState();
}

UMaterialInstance* ACBoardTile::GetColorMaterial() const
{
	return TileColorMaterials[static_cast<uint8>(TileColor)];
}

UMaterialInstance* ACBoardTile::GetMarkerMaterial() const
{
	return MarkerMaterials[static_cast<uint8>(TileState)];
}
