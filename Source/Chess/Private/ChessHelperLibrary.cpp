﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ChessHelperLibrary.h"

FString UChessHelperLibrary::LocationToSquareName(const FIntVector2& InLocation)
{
	const char Column = 'a' + InLocation.X;
	const char Row = '1' + InLocation.Y;
	
	return  FString::Printf(TEXT("%c%c"), Column, Row);
}

FIntVector2 UChessHelperLibrary::SquareNameToLocation(const FString& InNotation)
{
	const int32 x = InNotation[0] - 'a';
	const int32 y = InNotation[1] - '1';
	return {x, y};
}

EBoardColor UChessHelperLibrary::GetOppositeColor(const EBoardColor InColor)
{
	switch (InColor)
	{
	case EBoardColor::White:
		return EBoardColor::Black;

	case EBoardColor::Black:
		return EBoardColor::White;

	default:
		return EBoardColor::White;
	}
}

template< typename T >
FString EnumToString( T EnumValue )
{
	static_assert( TIsUEnumClass< T >::Value, "'T' template parameter to EnumToString must be a valid UEnum" );
	return StaticEnum< T >()->GetNameStringByIndex( ( int32 ) EnumValue );
}

FString UChessHelperLibrary::GameStausToText(const EGameStatus InStatus)
{
	return EnumToString(InStatus);
}
