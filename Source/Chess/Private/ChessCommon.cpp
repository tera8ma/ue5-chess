﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ChessCommon.h"

FString LocationToChessNotation(const FIntVector2& InLocation)
{
	const char Column = 'a' + InLocation.X;
	const char Row = '1' + InLocation.Y;

	return FString::Printf(TEXT("%c%c"), Column, Row);
}
