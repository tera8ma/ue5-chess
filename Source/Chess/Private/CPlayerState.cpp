﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "CPlayerState.h"

#include "Net/UnrealNetwork.h"

EBoardColor ACPlayerState::GetPlayerColor() const
{
	return PlayerColor;
}

void ACPlayerState::SetPlayerColor(const EBoardColor InColor)
{
	PlayerColor = InColor;
}

void ACPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps ) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ACPlayerState, PlayerColor);
}
