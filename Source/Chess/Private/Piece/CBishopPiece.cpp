﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Piece/CBishopPiece.h"

#include "CBoard.h"
#include "CEBoard.h"
#include "ChessHelperLibrary.h"

TArray<FString> ACBishopPiece::GetAvailableMoveNotations(const ACBoard* InBoard, const FIntVector2 InLocation) const
{
	return InBoard->GetCEBoard()->GetAvailableBishopMoves(UChessHelperLibrary::LocationToSquareName(InLocation));
}
