﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Piece/CRookPiece.h"

#include "CBoard.h"
#include "CEBoard.h"
#include "ChessHelperLibrary.h"

TArray<FString> ACRookPiece::GetAvailableMoveNotations(const ACBoard* InBoard, const FIntVector2 InLocation) const
{
	return InBoard->GetCEBoard()->GetAvailableRookMoves(UChessHelperLibrary::LocationToSquareName(InLocation));	
}
