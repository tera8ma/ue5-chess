﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Piece/CQueenPiece.h"

#include "CBoard.h"
#include "CEBoard.h"
#include "ChessHelperLibrary.h"

TArray<FString> ACQueenPiece::GetAvailableMoveNotations(const ACBoard* InBoard, const FIntVector2 InLocation) const
{
	return InBoard->GetCEBoard()->GetAvailableQueenMoves(UChessHelperLibrary::LocationToSquareName(InLocation));
}
