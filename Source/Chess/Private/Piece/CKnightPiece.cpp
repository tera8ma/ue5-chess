﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Piece/CKnightPiece.h"

#include "CBoard.h"
#include "CEBoard.h"
#include "ChessHelperLibrary.h"

TArray<FString> ACKnightPiece::GetAvailableMoveNotations(const ACBoard* InBoard, const FIntVector2 InLocation) const
{
	return InBoard->GetCEBoard()->GetAvailableKnightMoves(UChessHelperLibrary::LocationToSquareName(InLocation));
}
