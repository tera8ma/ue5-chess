﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Piece/CKingPiece.h"

#include "CBoard.h"
#include "CEBoard.h"
#include "ChessHelperLibrary.h"

TArray<FString> ACKingPiece::GetAvailableMoveNotations(const ACBoard* InBoard, const FIntVector2 InLocation) const
{
	return InBoard->GetCEBoard()->GetAvailableKingMoves(UChessHelperLibrary::LocationToSquareName(InLocation));
}
