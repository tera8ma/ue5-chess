﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Piece/CPawnPiece.h"

#include "CBoard.h"
#include "CEBoard.h"
#include "ChessHelperLibrary.h"

TArray<FString> ACPawnPiece::GetAvailableMoveNotations(const ACBoard* InBoard, const FIntVector2 InLocation) const
{
	return InBoard->GetCEBoard()->GetAvailablePawnMoves(UChessHelperLibrary::LocationToSquareName(InLocation));
}
