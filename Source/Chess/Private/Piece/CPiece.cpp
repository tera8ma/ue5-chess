// Fill out your copyright notice in the Description page of Project Settings.


#include "Piece/CPiece.h"

#include "CBoard.h"
#include "ChessHelperLibrary.h"

// Sets default values
ACPiece::ACPiece()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	SetRootComponent(MeshComponent);
}

void ACPiece::SetIsBlack(const bool InValue)
{
	bIsBlack = InValue;

	MeshComponent->SetMaterial(0, ColorMaterials[static_cast<uint8>(GetColor())]);
}

void ACPiece::MoveTo(const FVector& InNewLocation)
{
	BP_MoveToLocation(InNewLocation);
}

EBoardColor ACPiece::GetColor() const
{
	return bIsBlack ? EBoardColor::Black : EBoardColor::White;
}

TArray<FIntVector2> ACPiece::GetAvailableMoves(const ACBoard* InBoard, const FIntVector2 InLocation) const
{
	const TArray<FString> Moves = GetAvailableMoveNotations(InBoard, InLocation);
	TArray<FIntVector2> OutputLocations;
	OutputLocations.Reserve(Moves.Num());
	Algo::Transform(Moves, OutputLocations, &UChessHelperLibrary::SquareNameToLocation);

	return OutputLocations;
}

FString ACPiece::GetPieceSymbol() const
{
	return PieceSymbol;
}

void ACPiece::SetPieceType(const ECPieceType InType)
{
	if (InType != PieceType)
	{
		PieceType = InType;

		UStaticMesh* PieceMesh = PieceMeshes[static_cast<uint8>(InType)].LoadSynchronous();
		MeshComponent->SetStaticMesh(PieceMesh);
	}
}

ECPieceType ACPiece::GetPieceType() const
{
	return PieceType;
}

TArray<FString> ACPiece::GetAvailableMoveNotations(const ACBoard* InBoard, const FIntVector2 InLocation) const
{
	const UCEBoard* Board = InBoard->GetCEBoard();
	const FString& SquareName = UChessHelperLibrary::LocationToSquareName(InLocation);
	switch (PieceType)
	{
	case ECPieceType::Pawn: return Board->GetAvailablePawnMoves(SquareName);
	case ECPieceType::Knight: return Board->GetAvailableKnightMoves(SquareName);
	case ECPieceType::Bishop: return Board->GetAvailableBishopMoves(SquareName);
	case ECPieceType::Rook: return Board->GetAvailableRookMoves(SquareName);
	case ECPieceType::Queen: return Board->GetAvailableQueenMoves(SquareName);
	case ECPieceType::King: return Board->GetAvailableKingMoves(SquareName);
	default: return {};
	}
}

