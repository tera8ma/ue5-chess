﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "CBoard.h"

#include "CBoardTile.h"
#include "CEBoard.h"
#include "ChessGameState.h"
#include "ChessHelperLibrary.h"
#include "CPlayerController.h"
#include "CPlayerState.h"
#include "MagicConcatWrapper.h"
#include "Kismet/GameplayStatics.h"
#include "Piece/CKingPiece.h"
#include "Piece/CPiece.h"


// Sets default values
ACBoard::ACBoard()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootSceneComp = CreateDefaultSubobject<USceneComponent>(TEXT("SceneRoot"));
	SetRootComponent(RootSceneComp);

	WhitePileLocationComp = CreateDefaultSubobject<USceneComponent>(TEXT("WhitePile"));
	WhitePileLocationComp->SetupAttachment(RootSceneComp);
	
	BlackPileLocationComp = CreateDefaultSubobject<USceneComponent>(TEXT("BlackPile"));
	BlackPileLocationComp->SetupAttachment(RootSceneComp);
}

// Called when the game starts or when spawned
void ACBoard::BeginPlay()
{
	Super::BeginPlay();

	CEBoard = NewObject<UCEBoard>();
}

const UCEBoard* ACBoard::GetCEBoard() const
{
	return CEBoard;
}


void ACBoard::SpawnPiece(const ECPieceType InPieceType, const bool InIsBlack, const int32 InCol, const int32 InRow)
{
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	ACPiece* Piece = GetWorld()->SpawnActor<ACPiece>(PieceBP.LoadSynchronous(), GetTileLocation(InCol, InRow), FRotator::ZeroRotator, SpawnParameters);
	check(Piece);

	Piece->SetIsBlack(InIsBlack);
	Piece->SetPieceType(InPieceType);
	Pieces.Add({InCol, InRow}, Piece);
}

void ACBoard::SetTileStatePossibleMove(const FIntVector2& InLocation, ACBoardTile* InTile)
{	
	if (Pieces.Find(InLocation) == nullptr)
	{
		InTile->SetTileState(EBoardTileState::HighlightMove);
	}
}

void ACBoard::SetTileStateAttackMove(const FIntVector2& InLocation, ACBoardTile* InTile)
{
	if (TObjectPtr<ACPiece>* FoundPieceOnTile = Pieces.Find(InLocation))
	{
		TObjectPtr<ACPiece> Piece = *FoundPieceOnTile;
		const EBoardColor PieceColor = Piece->GetColor();

		const ACPlayerState* PlayerState = GetCurrentPlayerController()->GetPlayerState<ACPlayerState>();
		const EBoardColor PlayerColor = PlayerState->GetPlayerColor();
		if (PlayerColor != PieceColor)
		{
			InTile->SetTileState(EBoardTileState::Attackable);
		}
	}
}

void ACBoard::ResetTileState(ACBoardTile* InTile)
{
	InTile->SetTileState(EBoardTileState::None);
}

USceneComponent* ACBoard::GetPileComponentByColor(const EBoardColor InColor) const
{
	return InColor == EBoardColor::White ? WhitePileLocationComp : BlackPileLocationComp;
}

TArray<ACPiece*>& ACBoard::GetPileArrayByColor(const EBoardColor InColor)
{
	return InColor == EBoardColor::White ? DeadWhite : DeadBlack;
}

ACPlayerController* ACBoard::GetCurrentPlayerController() const
{
	return Cast<ACPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
}

FVector ACBoard::GetTileLocation(const int32 InCol, const int32 InRow) const
{
	check(InCol >= 0 && InRow >= 0);
	
	return GetActorLocation() + InCol * TileSize * GetActorRightVector() + InRow * TileSize * GetActorForwardVector();
}

FIntVector2 ACBoard::GetTileCoordinates(const ACBoardTile* InTile) const
{
	const FIntVector2* Coordinates = Tiles.FindKey(const_cast<ACBoardTile*>(InTile));
	if (Coordinates == nullptr)
	{
		return {-1, -1};
	}
	return *Coordinates;
}

void ACBoard::HighlightMoves(const ACPiece* InPiece)
{
	if (InPiece == nullptr)
	{
		return;
	}

	const FIntVector2* PieceLocation = Pieces.FindKey(const_cast<ACPiece*>(InPiece));
	if (PieceLocation == nullptr)
	{
		return;
	}

	AvailableMoves = InPiece->GetAvailableMoves(this, *PieceLocation);

	for (const FIntVector2& Location : AvailableMoves)
	{
		const TObjectPtr<ACBoardTile> Tile = Tiles.FindChecked(Location);
		const TObjectPtr<ACPiece>* FoundPieceOnTile = Pieces.Find(Location);
		if (FoundPieceOnTile == nullptr)
		{
			Tile->SetTileState(EBoardTileState::HighlightMove);
		}
		else if ((*FoundPieceOnTile)->GetColor() != InPiece->GetColor())
		{
			Tile->SetTileState(EBoardTileState::Attackable);
		}
	}
}

void ACBoard::ResetHighlight()
{
	for (const FIntVector2& Coord : AvailableMoves)
	{
		ResetTileState(Tiles.FindChecked(Coord));
	}
	
	AvailableMoves.Empty();
}

const ACPiece* ACBoard::GetPieceAt(const FIntVector2& InLocation) const
{
	const TObjectPtr<ACPiece>* Piece = Pieces.Find(InLocation);
	return Piece == nullptr ? nullptr : *Piece;
}

void ACBoard::SpawnPawns()
{
	for (int32 i = 0; i < BoardCols; ++i)
	{
		constexpr int32 WhiteRow = 1;
		SpawnPiece(ECPieceType::Pawn, false , i, WhiteRow);
	}

	for (int32 i = 0; i < BoardCols; ++i)
	{
		const int32 BlackRow = BoardRows - 2;
		SpawnPiece(ECPieceType::Pawn, true, i, BlackRow);
	}
}

void ACBoard::SpawnKings()
{
	SpawnPiece(ECPieceType::King, false, 4, 0);
	SpawnPiece(ECPieceType::King, true, 4, BoardRows - 1);
}

void ACBoard::SpawnKnights()
{
	SpawnPiece(ECPieceType::Knight, false, 1, 0);
	SpawnPiece(ECPieceType::Knight, false, BoardCols - 2, 0);
	
	SpawnPiece(ECPieceType::Knight, true, 1, BoardRows - 1);
	SpawnPiece(ECPieceType::Knight, true, BoardCols - 2, BoardRows - 1);
}

void ACBoard::SpawnQueens()
{
	SpawnPiece(ECPieceType::Queen, false, 3, 0);
	SpawnPiece(ECPieceType::Queen, true, 3, BoardRows - 1);
}

void ACBoard::SpawnBishops()
{
	SpawnPiece(ECPieceType::Bishop, false, 2, 0);
	SpawnPiece(ECPieceType::Bishop, false, BoardCols - 3, 0);
	
	SpawnPiece(ECPieceType::Bishop, true, 2, BoardRows - 1);
	SpawnPiece(ECPieceType::Bishop, true, BoardCols - 3, BoardRows - 1);
}

void ACBoard::SpawnRooks()
{
	SpawnPiece(ECPieceType::Rook, false, 0, 0);
	SpawnPiece(ECPieceType::Rook, false, BoardCols - 1, 0);
	
	SpawnPiece(ECPieceType::Rook, true, 0, BoardRows - 1);
	SpawnPiece(ECPieceType::Rook, true, BoardCols - 1, BoardRows - 1);
}

ACBoardTile* ACBoard::SpawnTileActor(const int32 InCol, const int32 InRow)
{
	if (TileBP.IsNull())
	{
		return nullptr;
	}

	const FVector& Location = GetTileLocation(InCol, InRow);
	ACBoardTile* TileActor = GetWorld()->SpawnActor<ACBoardTile>(TileBP.LoadSynchronous(), Location, FRotator::ZeroRotator, {});
	Tiles.Add(FIntVector2{InCol, InRow}, TileActor);

#if WITH_EDITOR
	TileActor->BP_SetText(UChessHelperLibrary::LocationToSquareName({InCol, InRow}));
#endif
	
	return TileActor;
}

void ACBoard::RemovePieceFromBoard(const TObjectPtr<ACPiece> InPiece)
{
	const USceneComponent* PileComponent = GetPileComponentByColor(InPiece->GetColor());
	const FVector& ForwardVector = PileComponent->GetForwardVector();
	TArray<ACPiece*>& Pile = GetPileArrayByColor(InPiece->GetColor());

	const int32 PileCount = Pile.Num();
	constexpr float PileOffset = 100.0f;
	InPiece->SetSmoothPosition(PileComponent->GetComponentLocation() + ForwardVector * PileCount * PileOffset);

	if (const FIntVector2* Key = Pieces.FindKey(InPiece))
	{
		Pieces.Remove(*Key);
	}


	Pile.Add(InPiece);
}

ECEMoveType ACBoard::GetMoveType(const FIntVector2& InFromLocation, const FIntVector2& InToLocation) const
{
	if (InFromLocation == InToLocation)
	{
		return ECEMoveType::None;
	}
	if (Pieces.Find(InToLocation) != nullptr)
	{
		return ECEMoveType::Capture;
	}

	const ACPiece* CurrentPiece = nullptr;
	const ACPiece* CapturedPiece = nullptr;
	
	if (const TObjectPtr<ACPiece>* Piece = Pieces.Find(InFromLocation))
	{
		CurrentPiece = *Piece;
	}
	if (const TObjectPtr<ACPiece>* Piece = Pieces.Find({InToLocation.X, InToLocation.Y - 1}))
	{
		CapturedPiece = *Piece;
	}

	if (CapturedPiece
		&& FMath::Abs(InToLocation.X - InFromLocation.X) == 1 
		&& CurrentPiece->GetPieceType() == ECPieceType::Pawn
		&& CapturedPiece->GetPieceType() == ECPieceType::Pawn
		&& CurrentPiece->GetColor() != CapturedPiece->GetColor())
	{
		return ECEMoveType::EnPassant;
	}

	if (CurrentPiece->GetPieceSymbol() == TEXT("K") && FMath::Abs(InFromLocation.X - InToLocation.X) > 1)
	{
		if (InFromLocation.X < InToLocation.X)
		{
			return ECEMoveType::CastleLong;
		}
		return ECEMoveType::CastleShort;
	}

	return ECEMoveType::Ordinary;
}

void ACBoard::MakeMoveInternal(const FIntVector2 FoundOldPosition, const FIntVector2 DesiredPosition, const ECEMoveType InMoveType, const ECPieceType InPieceType)
{
	ACPiece* InPiece = Pieces.FindChecked(FoundOldPosition);
	
	switch (InMoveType)
	{
	case ECEMoveType::Capture:
		RemovePieceFromBoard(Pieces.FindChecked(DesiredPosition));
		break;

	case ECEMoveType::Ordinary:
		break;

	case ECEMoveType::CastleLong: {
		ACPiece* LongRook = Pieces.FindChecked({ 7, FoundOldPosition.Y});
		Pieces.Remove({ 7, FoundOldPosition.Y});
		Pieces.Add({5, FoundOldPosition.Y}, LongRook);
		LongRook->SetSmoothPosition(GetTileLocation(5, FoundOldPosition.Y) + FVector(0, 0, 1));
		}
		break;

	case ECEMoveType::CastleShort: {
		ACPiece* ShortRook = Pieces.FindChecked({ 0, FoundOldPosition.Y});
		Pieces.Remove({ 0, FoundOldPosition.Y});
		Pieces.Add({3, FoundOldPosition.Y}, ShortRook);
		ShortRook->SetSmoothPosition(GetTileLocation(3, FoundOldPosition.Y) + FVector(0, 0, 1));
	}
		break;

	case ECEMoveType::EnPassant:
		RemovePieceFromBoard(Pieces.FindChecked({DesiredPosition.X, DesiredPosition.Y - 1}));
		break;

	case ECEMoveType::Promotion:
		InPiece->SetPieceType(InPieceType);
		break;

	case ECEMoveType::None:
		break;

	default:
		ensureAlwaysMsgf(false, TEXT("Unknown move type"));
		break;
	}

	if (FoundOldPosition != DesiredPosition)
	{
		Pieces.Remove(FoundOldPosition);
		Pieces.Add(DesiredPosition, InPiece);
	}

	InPiece->SetSmoothPosition(GetTileLocation(DesiredPosition.X, DesiredPosition.Y) + FVector(0, 0, 1));
}

void ACBoard::MakeMove(ACPiece* InPiece, ACBoardTile* InTile, const ECPieceType InPromotion)
{
	if (!ensure(InPiece))
	{
		return;
	}

#if WITH_EDITOR
	if (Pieces.FindKey(InPiece) == nullptr)
	{
		ensureAlwaysMsgf(false, TEXT("Failed to find piece"));
		return;
	}
#endif

	const FIntVector2 FoundOldPosition = *Pieces.FindKey(InPiece);
	const FIntVector2* NewPosition = Tiles.FindKey(InTile);

	FIntVector2 DesiredPosition = FoundOldPosition;

	if (NewPosition != nullptr && AvailableMoves.Contains(*NewPosition))
	{
		DesiredPosition = *NewPosition;
	}
	
	const ECEMoveType MoveType = InPromotion == ECPieceType::None
		? GetMoveType(FoundOldPosition, DesiredPosition)
		: ECEMoveType::Promotion;
	
	MakeMoveInternal(FoundOldPosition, DesiredPosition, MoveType, InPromotion);

	if (MoveType != ECEMoveType::None)
	{
		const FCEMove PlayerMove = { FoundOldPosition, DesiredPosition, MoveType, static_cast<ECEPieceType>(InPromotion)};
		CEBoard->MakeMove(PlayerMove);

		SrvMakeMove(PlayerMove);
	}
	

#if WITH_EDITOR
	const FString FEN = CEBoard->GetForsythEdwardsNotation();
	UE_LOG(LogTemp, Log, TEXT("FEN: '%s'"), *FEN);
#endif
	
}

void ACBoard::SrvMakeMove_Implementation(const FCEMove& InMove)
{
	for (int32 i = 0; i < 2; ++i)
	{
		APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), i);
		if (PlayerController != GetOwner())
		{
			SetOwner(PlayerController);
			break;
		}
	}

	if (HasAuthority())
	{
		UE_LOG(LogTemp, Log, TEXT("Pre OnPlayerMove"));
	}

	OnPlayerMove(InMove);


	if (AChessGameState* ChessGameState = GetWorld()->GetGameState<AChessGameState>())
	{
		if (HasAuthority())
		{
			UE_LOG(LogTemp, Log, TEXT("Before set status"));
		}
		ChessGameState->ChangeActivePlayer();
		ChessGameState->SetGameStatus(static_cast<EGameStatus>(GetCEBoard()->GetGameStatus()))	;
	}
}

void ACBoard::OnPlayerMove_Implementation(const FCEMove& InMove)
{
	if (HasAuthority())
	{
		CEBoard->MakeMove(InMove);
	}
	const TObjectPtr<ACPiece>* FoundPiece = Pieces.Find(InMove.FromPosition);
	if (FoundPiece == nullptr)
	{
		return;
	}

	MakeMoveInternal(InMove.FromPosition, InMove.ToPosition, InMove.MoveType, static_cast<ECPieceType>(InMove.Promotion));
	CEBoard->MakeMove(InMove);
	
	if (HasAuthority())
	{
		UE_LOG(LogTemp, Log, TEXT("After OnPlayerMove"));
	}
}

void ACBoard::HoverTile(ACBoardTile* InTile)
{
	if (InTile == HoveredTile)
	{
		return;
	}
	
	if (HoveredTile)
	{
		HoveredTile->SetHover(false);
	}
	
	if (InTile)
	{
		InTile->SetHover(true);
	}
	
	HoveredTile = InTile;
}

