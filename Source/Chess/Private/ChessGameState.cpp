// Fill out your copyright notice in the Description page of Project Settings.


#include "ChessGameState.h"
#include "CEBoard.h"

#include "ChessHelperLibrary.h"
#include "Net/UnrealNetwork.h"

void AChessGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps ) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AChessGameState, CurrentActivePlayer);
	DOREPLIFETIME(AChessGameState, GameStatus);
}

void AChessGameState::ChangeActivePlayer()
{
	CurrentActivePlayer = UChessHelperLibrary::GetOppositeColor(CurrentActivePlayer);
}

EBoardColor AChessGameState::GetActivePlayerColor() const
{
	return CurrentActivePlayer;
}

EGameStatus AChessGameState::GetStatus() const
{
	return GameStatus;
}

void AChessGameState::OnRep_CurrentActivePlayer()
{
	OnCurrentPlayerChanged.Broadcast();
}

void AChessGameState::OnRep_GameStatus()
{
	OnStatusChanged.Broadcast();
}

void AChessGameState::BeginPlay()
{
	GameStatus = EGameStatus::InProgress;
	Super::BeginPlay();
}

void AChessGameState::SetGameStatus(const EGameStatus InGameStatus)
{
    if (InGameStatus != GameStatus)
    {
        GameStatus = InGameStatus;
    }
}
