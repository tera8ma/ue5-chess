// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ChessGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CHESS_API AChessGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSoftClassPtr<class ACBoard> BoardBP;

	UFUNCTION(BlueprintCallable)
	void MakeMove(class ACPiece* InPiece, class ACBoardTile* InTile);
	
protected:
	virtual void BeginPlay() override;
public:
	virtual void StartPlay() override;
	virtual void PostLogin(APlayerController* NewPlayer) override;
private:
	UPROPERTY(Transient)
	class ACBoard* Board;
	
};
