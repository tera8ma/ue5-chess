// Copyright Epic Games, Inc. All Rights Reserved.


#include "ChessGameModeBase.h"

#include "CBoard.h"
#include "CPlayerState.h"
#include "Kismet/GameplayStatics.h"

void AChessGameModeBase::MakeMove(ACPiece* InPiece, class ACBoardTile* InTile)
{
	if (Board)
	{
		Board->MakeMove(InPiece, InTile, ECPieceType::None);	
	}
}

void AChessGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	Board = CastChecked<ACBoard>(UGameplayStatics::GetActorOfClass(GetWorld(), ACBoard::StaticClass()));
	// Board = GetWorld()->SpawnActor<ACBoard>(BoardBP.LoadSynchronous());
}

void AChessGameModeBase::StartPlay()
{
	Super::StartPlay();
}

void AChessGameModeBase::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	if (NewPlayer)
	{
		ACPlayerState* PlayerState = NewPlayer->GetPlayerState<ACPlayerState>();
		PlayerState->SetPlayerColor(GetNumPlayers() <= 1 ? EBoardColor::White : EBoardColor::Black);
	}

	APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (ensure(PlayerController) && Board)
	{
		Board->SetOwner(PlayerController);
	}
}
