﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CPiece.h"
#include "CPawnPiece.generated.h"

UCLASS()
class CHESS_API ACPawnPiece : public ACPiece
{
	GENERATED_BODY()

public:
	virtual TArray<FString> GetAvailableMoveNotations(const ACBoard* InBoard, const FIntVector2 InLocation) const override;
};
