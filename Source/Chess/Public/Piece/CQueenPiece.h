﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CPiece.h"
#include "CQueenPiece.generated.h"

UCLASS()
class CHESS_API ACQueenPiece : public ACPiece
{
	GENERATED_BODY()

protected:
	virtual TArray<FString> GetAvailableMoveNotations(const ACBoard* InBoard, const FIntVector2 InLocation) const override;
};
