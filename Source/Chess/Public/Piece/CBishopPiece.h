﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CPiece.h"
#include "CBishopPiece.generated.h"

/**
 * 
 */
UCLASS()
class CHESS_API ACBishopPiece : public ACPiece
{
	GENERATED_BODY()
public:
	virtual TArray<FString> GetAvailableMoveNotations(const ACBoard* InBoard, const FIntVector2 InLocation) const override;
};