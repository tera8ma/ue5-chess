// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ChessCommon.h"
#include "GameFramework/Pawn.h"
#include "CPiece.generated.h"

DECLARE_DELEGATE_TwoParams(FPieceSelectedDelegate, class ACPiece*, const bool);

UENUM()
enum class ECPieceType : uint8
{
	None,
	Pawn,
	Knight,
	Bishop,
	Rook,
	Queen,
	King,
	MAX UMETA(Hidden)
};

UCLASS()
class CHESS_API ACPiece : public APawn
{
	GENERATED_BODY()

public:
	ACPiece();
	
	void SetIsBlack(const bool InValue);
	void MoveTo(const FVector& InNewLocation);
	EBoardColor GetColor() const;
	TArray<FIntVector2> GetAvailableMoves(const class ACBoard* InBoard, const FIntVector2 InLocation) const;
	FString GetPieceSymbol() const;
	void SetPieceType(const ECPieceType InType);
	ECPieceType GetPieceType() const;

	UFUNCTION(BlueprintImplementableEvent)
	void SetSmoothPosition(const FVector InDesiredPosition);

protected:
	virtual TArray<FString> GetAvailableMoveNotations(const class ACBoard* InBoard, const FIntVector2 InLocation) const;
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	TObjectPtr<UStaticMeshComponent> MeshComponent;

	UPROPERTY(EditAnywhere)
	ECPieceType PieceType = ECPieceType::Pawn;

	UPROPERTY(EditAnywhere)
	FString PieceSymbol;
	
	UPROPERTY(EditAnywhere)
	TArray<FIntVector2> MovePatterns;

	UPROPERTY(EditAnywhere)
	class UMaterialInstance* ColorMaterials[(uint8)EBoardColor::MAX];

	UPROPERTY(EditAnywhere)
	TSoftObjectPtr<UStaticMesh> PieceMeshes[(uint8)ECPieceType::MAX];

	UPROPERTY(EditAnywhere)
	bool bIsBlack = false;
	
	UFUNCTION(BlueprintImplementableEvent)
	void BP_MoveToLocation(const FVector& InNewLocation);
};
