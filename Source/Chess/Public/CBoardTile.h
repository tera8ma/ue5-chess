﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ChessCommon.h"
#include "GameFramework/Actor.h"
#include "CBoardTile.generated.h"

UENUM()
enum class EBoardTileState : uint8
{
	None,
	HighlightMove,
	Attackable,
	PieceSelected,
	MAX UMETA(Hidden)
};

UCLASS()
class CHESS_API ACBoardTile : public AActor
{
	GENERATED_BODY()

public:
	ACBoardTile();

	UFUNCTION(BlueprintCallable)
	void SetTileState(const EBoardTileState InNewState);

	UFUNCTION(BlueprintCallable)
	EBoardTileState GetTileState() const;

	UFUNCTION(BlueprintCallable)
	void SetTileColor(const EBoardColor InColor);

	UFUNCTION(BlueprintCallable)
	EBoardColor GetTileColor() const;

	void SetHover(const bool InValue);

	UFUNCTION(BlueprintImplementableEvent)
	void BP_SetText(const FString& InText);

protected:
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	TObjectPtr<UStaticMeshComponent> TileComponent;
	
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	TObjectPtr<UStaticMeshComponent> MarkerComponent;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	TObjectPtr<UStaticMeshComponent> HoverComponent;
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UFUNCTION(BlueprintImplementableEvent)
	void BP_UpdateVisualState();

	UFUNCTION(BlueprintCallable)
	UMaterialInstance* GetColorMaterial() const;

	UFUNCTION(BlueprintCallable)
	UMaterialInstance* GetMarkerMaterial() const;

	UPROPERTY(EditAnywhere)
	TObjectPtr<UMaterialInstance> MarkerMaterials[(uint8)EBoardTileState::MAX];

	UPROPERTY(EditAnywhere)
	TObjectPtr<UMaterialInstance> TileColorMaterials[(uint8)EBoardColor::MAX];

	UPROPERTY(EditAnywhere)
	EBoardColor TileColor = EBoardColor::White;

	UPROPERTY(EditAnywhere)
	EBoardTileState TileState = EBoardTileState::None;

};
