﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CBoardTile.h"
#include "CPlayerController.generated.h"

UCLASS()
class CHESS_API ACPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ACPlayerController();
	void SetInputMapping() const;
	void RemoveInputMapping() const;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(EditAnywhere)
	float HoverRefreshRate = 0.1;
	UPROPERTY(EditAnywhere)
	float DragHeightOffset = 100.0f;
	UPROPERTY(EditAnywhere)
	TSoftClassPtr<class UCPromotionWindow> PromotionWindowBP;
	UPROPERTY(EditAnywhere)
	class UInputMappingContext* InputMappingContext;

	UFUNCTION(BlueprintCallable)
	void TryStartDragging();
	void ShowPromotionWindow(class ACPiece* InPiece, class ACBoardTile* TileUnderCursor);
	UFUNCTION(BlueprintCallable)
	void StopDragging();
	

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void OnMouseHover() const;

private:
	FTimerHandle MouseHoveHandle;
	UPROPERTY(Transient)
	class ACBoard* BoardActor;
	UPROPERTY(Transient)
	class ACPiece* DraggingPiece;
	UPROPERTY(Transient)
	UCPromotionWindow* PromotionWindow;

	class ACBoard* GetBoard() const;
	bool CanMakeMove() const;
	bool CanMovePiece(const class ACPiece* InPiece) const;
};
