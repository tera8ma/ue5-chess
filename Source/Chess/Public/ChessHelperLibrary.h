﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ChessCommon.h"
#include "UObject/Object.h"
#include "ChessHelperLibrary.generated.h"

/**
 * 
 */
UCLASS()
class CHESS_API UChessHelperLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	static FString LocationToSquareName(const FIntVector2& InLocation);
	static FIntVector2 SquareNameToLocation(const FString& InNotation);
	static EBoardColor GetOppositeColor(const EBoardColor InColor);
	UFUNCTION(BlueprintCallable, BlueprintPure)
	static FString GameStausToText(const EGameStatus InStatus);
};
