// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ChessCommon.h"
#include "GameFramework/GameState.h"
#include "ChessGameState.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FChangedActivePlayer);

enum class ECEGameStatus : uint8;

/**
 * 
 */
UCLASS()
class CHESS_API AChessGameState : public AGameStateBase
{
	GENERATED_BODY()

public:
	void ChangeActivePlayer();
	EBoardColor GetActivePlayerColor() const;
	UFUNCTION(BlueprintCallable)
	EGameStatus GetStatus() const;
	
	UPROPERTY(BlueprintAssignable)
	FChangedActivePlayer OnCurrentPlayerChanged;

	UPROPERTY(BlueprintAssignable)
	FChangedActivePlayer OnStatusChanged;

	virtual void BeginPlay() override;
	
	void SetGameStatus(const EGameStatus InGameStatus);

protected:
	UPROPERTY(ReplicatedUsing=OnRep_CurrentActivePlayer, BlueprintReadOnly)
	EBoardColor CurrentActivePlayer = EBoardColor::White;
	
	UPROPERTY(ReplicatedUsing=OnRep_GameStatus, BlueprintReadonly)
	EGameStatus GameStatus;

	UFUNCTION()
	void OnRep_CurrentActivePlayer();
	
	UFUNCTION()
	void OnRep_GameStatus();
};
