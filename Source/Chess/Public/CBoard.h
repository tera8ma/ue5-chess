﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CBoardTile.h"
#include "CEBoard.h"
#include "GameFramework/Actor.h"
#include "Piece/CPiece.h"
#include "CBoard.generated.h"

class ACPlayerController;
class ACBoardTile;

UCLASS()
class CHESS_API ACBoard : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ACBoard();

	UFUNCTION(BlueprintCallable)
	FVector GetTileLocation(const int32 InCol, const int32 InRow) const;

	FIntVector2 GetTileCoordinates(const ACBoardTile* InTile) const;

	void HighlightMoves(const ACPiece* InPiece);
	void ResetHighlight();
	const ACPiece* GetPieceAt(const FIntVector2& InLocation) const;
	int32 GetRows() const { return BoardRows; }
	int32 GetCols() const { return BoardCols; }

	UFUNCTION(BlueprintCallable)
	void SpawnPawns();

	UFUNCTION(BlueprintCallable)
	void SpawnKings();

	UFUNCTION(BlueprintCallable)
	void SpawnKnights();

	UFUNCTION(BlueprintCallable)
	void SpawnQueens();

	UFUNCTION(BlueprintCallable)
	void SpawnBishops();

	UFUNCTION(BlueprintCallable)
	void SpawnRooks();

	UFUNCTION(BlueprintCallable)
	ACBoardTile* SpawnTileActor(const int32 InCol, const int32 InRow);

	void RemovePieceFromBoard(const TObjectPtr<ACPiece> InPiece);
	ECEMoveType GetMoveType(const FIntVector2& InFromLocation, const FIntVector2& InToLocation) const;
	void MakeMove(ACPiece* InPiece, ACBoardTile* InTile, const ECPieceType InPromotion);

	UFUNCTION(Server, Reliable)
	void SrvMakeMove(const struct FCEMove& InMove);
	
	UFUNCTION(NetMulticast, Reliable)
	void OnPlayerMove(const struct FCEMove& InMove);

	void HoverTile(ACBoardTile* InTile);

	const class UCEBoard* GetCEBoard() const;

protected:
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	class USceneComponent* WhitePileLocationComp;
	
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	class USceneComponent* BlackPileLocationComp;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	class USceneComponent* RootSceneComp;
	
	UPROPERTY(EditAnywhere, Category = Board)
	int32 BoardRows = 8;

	UPROPERTY(EditAnywhere, Category = Board)
	int32 BoardCols = 8;
	
	UPROPERTY(EditAnywhere, Category = Board)
	float TileSize = 100.0f;

	UPROPERTY(Transient)
	TArray<ACPiece*> DeadWhite;

	UPROPERTY(Transient)
	TArray<ACPiece*> DeadBlack;

	UPROPERTY(EditAnywhere, Category = "Board|Pieces")
	TSoftClassPtr<ACPiece> PieceBP;
	UPROPERTY(EditAnywhere, Category = "Board")
	TSoftClassPtr<ACBoardTile> TileBP;

	TArray<FIntVector2> AvailableMoves;
	
	virtual void BeginPlay() override;
private:
	UPROPERTY(Transient)
	TMap<FIntVector2, TObjectPtr<ACPiece>> Pieces;
	UPROPERTY(Transient)
	TMap<FIntVector2, TObjectPtr<ACBoardTile>> Tiles;
	UPROPERTY(Transient)
	TObjectPtr<ACBoardTile> HoveredTile;
	UPROPERTY(Transient)
	class UCEBoard* CEBoard;

	void SpawnPiece(const ECPieceType InPieceType, const bool InIsBlack, const int32 InCol, const int32 InRow);
	
	void SetTileStatePossibleMove(const FIntVector2& InLocation, ACBoardTile* InTile);
	void SetTileStateAttackMove(const FIntVector2& InLocation, ACBoardTile* InTile);
	void ResetTileState(ACBoardTile* InTile);

	USceneComponent* GetPileComponentByColor(const EBoardColor InColor) const;
	TArray<ACPiece*>& GetPileArrayByColor(const EBoardColor InColor);

	ACPlayerController* GetCurrentPlayerController() const;
	void MakeMoveInternal(const FIntVector2 FoundOldPosition, const FIntVector2 DesiredPosition, const ECEMoveType InMoveType, const ECPieceType InPieceType);
};
