﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Piece/CPiece.h"
#include "CPromotionWindow.generated.h"

DECLARE_DELEGATE_OneParam(FPromotionResult, ECPieceType);

/**
 * 
 */
UCLASS()
class CHESS_API UCPromotionWindow : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	void SetPromotionResult(const ECPieceType InPiece);
	
	FPromotionResult OnPromoted;
};
