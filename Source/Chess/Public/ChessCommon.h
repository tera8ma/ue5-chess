﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "ChessCommon.generated.h"

UENUM()
enum class EBoardColor : uint8
{
	White,
	Black,
	MAX UMETA(Hidden)
};

UENUM()
enum class EGameStatus : uint8
{
	WhiteWin,
	BlackWin,
	StaleMate,
	Check,
	Draw,
	InProgress
};

inline EBoardColor GetOppositeColor(const EBoardColor InColor)
{
	switch (InColor)
	{
	case EBoardColor::White:
		return EBoardColor::Black;

	case EBoardColor::Black:
	default:
		return EBoardColor::White;
	}
}

