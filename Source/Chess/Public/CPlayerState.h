﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ChessCommon.h"
#include "GameFramework/PlayerState.h"
#include "UObject/Object.h"
#include "CPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class CHESS_API ACPlayerState : public APlayerState
{
	GENERATED_BODY()

public:
	EBoardColor GetPlayerColor() const;
	
protected:
	void SetPlayerColor(const EBoardColor InColor);
	UPROPERTY(Replicated, BlueprintReadOnly)
	EBoardColor PlayerColor = EBoardColor::White;


	friend  class AChessGameModeBase;
};
